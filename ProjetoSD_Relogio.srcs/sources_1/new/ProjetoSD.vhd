library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;

entity ProjetoSD is
GENERIC (SEG: INTEGER := 49999999);
    Port ( CLOCK : in STD_LOGIC:='0';
           Led : out STD_LOGIC_VECTOR ( 4 downto 0):= "00000";
           DISP_ATIVO : out  STD_LOGIC_VECTOR(3 downto 0):="0000";
           CLOCK_PONTO: out STD_LOGIC:='0';
           CONTROLE_MP: out STD_LOGIC_VECTOR(3 downto 0):="0000";
              --CONTROLE_MP(0)=FIO AZUL
              --CONTROLE_MP(1)=FIO AZUL
              --CONTROLE_MP(2)=FIO AZUL
              --CONTROLE_MP(3)=FIO AZUL
           AJUSTA_MINUTOS: in STD_LOGIC:='0';
           AJUSTA_HORAS: in STD_LOGIC:='0';
           segmento : out STD_LOGIC_VECTOR(6 downto 0):="0000000"
           );
           
            SIGNAL CLOCK_AUX : STD_LOGIC:='0';
            SIGNAL CLOCKDISPLAY : STD_LOGIC:='0';
            SIGNAL CLOCKDISPLAY2 : STD_LOGIC:='0';
            SIGNAL CONTADOR : STD_LOGIC_VECTOR(25 downto 0):="00000000000000000000000000";
            SIGNAL CONTADORDISP : STD_LOGIC_VECTOR(16 downto 0):="00000000000000000";
            
end ProjetoSD;

architecture Behavioral of ProjetoSD is
        SIGNAL TEMPORAL    : INTEGER RANGE 0 TO SEG:= 0;
        SIGNAL SEGUNDOS    : INTEGER RANGE 0 TO 59 := 0; -- SEGUNDOS
        SIGNAL HORAS_UNI   : INTEGER RANGE 0 TO 9  := 0; -- CARACTERE DA HORA UNIDADE
        SIGNAL HORAS_DEZ   : INTEGER RANGE 0 TO 2  := 0; -- CARACTERE DA HORA DEZENA
        SIGNAL MINUTOS_UNI : INTEGER RANGE 0 TO 9  := 0; -- CARACTERE DE MINUTO UNIDADE
        SIGNAL MINUTOS_DEZ : INTEGER RANGE 0 TO 5  := 0; -- CARACTERE DE MINUTO DEZENA
        
       
begin
--------------------------------------
  DivideClockDisplay: process(CLOCK)
          begin
              if (rising_edge(CLOCK)) then 
                       CONTADORDISP <= CONTADORDISP+1;
                    end if;
                    CLOCKDISPLAY <=CONTADORDISP(16);
                    CLOCKDISPLAY2 <=CONTADORDISP(15); 
          END PROCESS;
--------------------------------------
  MostraDisplay: process(CLOCKDISPLAY)
           begin
           
              if(CLOCKDISPLAY = '0' AND CLOCKDISPLAY2 = '0') then
                    CLOCK_PONTO <='1';
                            if(MINUTOS_UNI = 0)then 
                                 -- segmento <= "1111110";
                                 segmento <="0000001";
                            end if;
                            if(MINUTOS_UNI = 1)then 
                                 -- segmento <= "0110000";
                               segmento  <= "1001111";   
                            end if;
                            if(MINUTOS_UNI = 2)then 
                                  --segmento <= "1101101";
                                 segmento <="0010010";
                            end if;
                            if(MINUTOS_UNI = 3)then 
                                 -- segmento <= "1111001";
                                segmento <= "0000110";
                            end if;
                             if(MINUTOS_UNI = 4)then 
                                 -- segmento <= "0110011";
                                segmento <= "1001100";
                            end if;
                             if(MINUTOS_UNI = 5)then 
                                --  segmento <= "1011011";
                                segmento <= "0100100";
                            end if;
                             if(MINUTOS_UNI = 6)then 
                                --  segmento <= "1011111";
                                segmento <= "0100000";
                            end if;
                             if(MINUTOS_UNI = 7)then 
                                --  segmento <= "1110000";
                                segmento <= "0001111";
                            end if;
                             if(MINUTOS_UNI = 8)then 
                                --  segmento <= "1111111";
                                segmento <= "0000000";
                            end if;
                             if(MINUTOS_UNI = 9)then 
                                --  segmento <= "1111011";
                                segmento <= "0000100";
                             end if;
                                              
                                  DISP_ATIVO(0) <= '0';
                                  DISP_ATIVO(1) <= '1';
                                  DISP_ATIVO(2) <= '1';
                                  DISP_ATIVO(3) <= '1';  
              end if;
             
               if(CLOCKDISPLAY = '0' AND CLOCKDISPLAY2 = '1') then
                             if(MINUTOS_DEZ = 0)then 
                                --segmento <= "1111110";
                                segmento <="0000001";
                            end if;
                            if(MINUTOS_DEZ = 1)then 
                                --segmento <= "0110000";
                                segmento  <= "1001111";   
                            end if;
                            if(MINUTOS_DEZ = 2)then 
                                --segmento <= "1101101";
                                segmento <="0010010";
                            end if;
                            if(MINUTOS_DEZ = 3)then 
                                --segmento <= "1111001";
                                segmento <= "0000110";
                            end if;
                            if(MINUTOS_DEZ = 4)then 
                                --segmento <= "0110011";
                                segmento <= "1001100";
                            end if;
                            if(MINUTOS_DEZ = 5)then 
                                --segmento <= "1011011";
                                segmento <= "0100100";
                            end if;
                                  DISP_ATIVO(0) <= '1';
                                  DISP_ATIVO(1) <= '0';
                                  DISP_ATIVO(2) <= '1';
                                  DISP_ATIVO(3) <= '1';
                          end if;
                --DISPLAY DAS HORAS UNIDADE
                if(CLOCKDISPLAY = '1' AND CLOCKDISPLAY2 = '0') then
                            
                            if(HORAS_UNI = 0)then 
                              -- segmento <= "1111110";
                                segmento <="0000001";
                            end if;
                            if(HORAS_UNI = 1)then 
                             -- segmento <= "0110000";
                                segmento  <= "1001111";   
                            end if;
                            if(HORAS_UNI = 2)then 
                           --   segmento <= "1101101";
                                segmento <="0010010";
                            end if;
                            if(HORAS_UNI = 3)then 
                           --  segmento <= "1111001";
                                segmento <= "0000110";
                            end if;
                            if(HORAS_UNI = 4)then 
                            --segmento <= "0110011";
                                segmento <= "1001100";
                            end if;
                            if(HORAS_UNI = 5)then 
                            --  segmento <= "1011011";
                                segmento <= "0100100";
                            end if;
                            if(HORAS_UNI = 6)then 
                            --  segmento <= "1011111";
                                segmento <= "0100000";
                            end if;
                            if(HORAS_UNI = 7)then 
                            --  segmento <= "1110000";
                                segmento <= "0001111";
                            end if;
                            if(HORAS_UNI = 8)then 
                           --  segmento <= "1111111";
                                segmento <= "0000000";
                            end if;
                            if(HORAS_UNI = 9)then 
                           --  segmento <= "1111011";
                                segmento <= "0000100";
                            end if;
                                  DISP_ATIVO(0) <= '1';
                                  DISP_ATIVO(1) <= '1';
                                  DISP_ATIVO(2) <= '0';
                                  DISP_ATIVO(3) <= '1';
                          end if; 
                                      
                 if(CLOCKDISPLAY = '1' AND CLOCKDISPLAY2 = '1') then
                             if(HORAS_DEZ = 0)then 
                                  --  segmento <= "1111110";
                                  segmento <="0000001";
                            end if;
                            if(HORAS_DEZ = 1)then 
                                  --  segmento <= "0110000";
                                  segmento  <= "1001111";   
                             end if;
                            if(HORAS_DEZ = 2)then 
                                    -- segmento <= "1101101";
                                  segmento <="0010010";
                             end if;
                                   DISP_ATIVO(0) <= '1';
                                   DISP_ATIVO(1) <= '1';
                                   DISP_ATIVO(2) <= '1';
                                   DISP_ATIVO(3) <= '0';
                 end if;
                  
           END PROCESS;
 ---------------------------------------
  DivideClockPlaca: process(CLOCK)
          begin
             if (rising_edge(CLOCK)) then 
                CONTADOR <= CONTADOR+1;
             end if;
             CLOCK_AUX <=CONTADOR(25);
          END PROCESS;

  Tempo:  process(CLOCK_AUX)
        begin 
          IF rising_edge(CLOCK_AUX) THEN -- NA BORDA DE SUBIDA
                
               --IF(TEMPORAL /= SEG) THEN  
                 --    TEMPORAL <= TEMPORAL + 1; -- INCREMENTA 
                
               --ELSE 
                if(AJUSTA_MINUTOS = '1') then
                       MINUTOS_UNI <= MINUTOS_UNI+1;
                           IF(MINUTOS_UNI = 9) then
                               MINUTOS_DEZ <=MINUTOS_DEZ+1;
                           end if;
                 end if;
        
                                        --ELSE 
                if(AJUSTA_HORAS = '1') then
                       HORAS_UNI <= HORAS_UNI+1;
                           IF(HORAS_UNI = 9) then
                               HORAS_DEZ <=HORAS_DEZ+1;
                           end if;
                 end if;
                     TEMPORAL <= 0;
                     SEGUNDOS <= SEGUNDOS + 1;
           IF(SEGUNDOS = 0) THEN
                CONTROLE_MP <="1001";
           END IF;  
            
           IF(SEGUNDOS = 15) THEN
                CONTROLE_MP <="1100";
           END IF;     
          
           IF(SEGUNDOS = 30) THEN
                CONTROLE_MP <="0110";
           END IF;
           
           IF(SEGUNDOS = 45) THEN
                 CONTROLE_MP <="0011";
           END IF;    
           
           IF(SEGUNDOS = 9) THEN                          
                                  Led(0) <='1';
                                  Led(1) <='0';
                                  Led(2) <='0';
                                  Led(3) <='0';
                                  Led(4) <='0';
             END IF;
             IF(SEGUNDOS = 19) THEN                                
                                   Led(0) <='1';
                                   Led(1) <='1';
                                   Led(2) <='0';
                                   Led(3) <='0';
                                   Led(4) <='0';
              END IF;
              IF(SEGUNDOS = 29) THEN                                          
                                   Led(0) <='1';
                                   Led(1) <='1';
                                   Led(2) <='1';
                                   Led(3) <='0';
                                   Led(4) <='0';
               END IF;
               IF(SEGUNDOS = 39) THEN                                                    
                                   Led(0) <='1';
                                   Led(1) <='1';
                                   Led(2) <='1';
                                   Led(3) <='1';
                                   Led(4) <='0';
                END IF;
                IF(SEGUNDOS = 49) THEN                                                               
                                  Led(0) <='1';
                                  Led(1) <='1';
                                  Led(2) <='1';
                                  Led(3) <='1';
                                  Led(4) <='1';
                 END IF;
          
         IF(SEGUNDOS = 59) THEN         --ZERA O SEGUNDOS E OS LEDS PARA CONTAGEM DO NOVO MINUTO 

                              SEGUNDOS <= 0;
                               Led(0) <='0';
                               Led(1) <='0';
                               Led(2) <='0';
                               Led(3) <='0';
                               Led(4) <='0';
                  MINUTOS_UNI <= MINUTOS_UNI + 1;
          
          IF(MINUTOS_UNI = 9) THEN 
           MINUTOS_UNI <= 0; -- CASO PASSE 10 MINUTOS RESETA O DISPLAY DE UNIDADES DOS MINUTOS
           MINUTOS_DEZ <= MINUTOS_DEZ + 1; 
           
           IF(MINUTOS_DEZ = 5) THEN 
            MINUTOS_DEZ <= 0;
            HORAS_UNI <= HORAS_UNI + 1;
            
            IF(HORAS_UNI = 9) THEN
             HORAS_UNI <= 0;
             HORAS_DEZ <= HORAS_DEZ + 1;
             
             ELSIF (HORAS_UNI = 3 AND HORAS_DEZ = 2) THEN--QUANDO FOR 23 HORAS  RESETA(JA TEM A CONDICAO PARA O 59)
              HORAS_DEZ <= 0;
              HORAS_UNI <= 0;
              MINUTOS_DEZ <= 0;
              MINUTOS_UNI <= 0;
            END IF;
           END IF;
          END IF;
         END IF;
        END IF;
       --END IF; 
    END PROCESS;
 --==================================================================================================================
end Behavioral;
